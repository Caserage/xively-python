#!/usr/bin/env python
# -*- coding: utf-8 -*-

import threading
import time
import json
import http.client as httplib
import logging as log


class Xively:
    HOST = 'api.xively.com'
    AGENT = 'Xively Client'
    FEED = '1554587897.json'
    API_KEY = 'fVDWSflks5yK0aZfSQcdTTHvuxMLdlKk0G2r3EGZ0nwFFlQ9'

    def __init__(self):
        self.items = {}
        self.headers = {
            "Content-Type": "application/json charset=utf-8",
            "X-ApiKey": self.API_KEY,
            "User-Agent": Xively.AGENT,
        }
        self.params = "/v2/feeds/" + str(self.FEED)
        self.upload_thread = threading.Thread(target=self.upload)
        self.upload_thread.daemon = True
        self.upload_thread.start()

    def put(self, identifier, value):
        try:
            _, min_value, max_value = self.items[identifier]
            if value < min_value:
                min_value = value
            if value > max_value:
                max_value = value
            self.items[identifier] = (value, min_value, max_value)
        except:
            self.items[identifier] = (value, value, value)

    def upload(self):
        while True:
            time.sleep(5 * 60)
            if len(self.items) == 0:
                continue

            stream_items = []
            for identifier, value in self.items.items():
                stream_items.append({'id': identifier,
                                     'current_value': value[0],
                                     'min_value': value[1],
                                     'max_value': value[2]})

                data = {'version': '1.0.0',
                        'datastreams': stream_items}
                self.items = {}
                body = json.dumps(data)

                try:
                    http = httplib.HTTPSConnection(Xively.HOST)
                    http.request('PUT', self.params, body, self.headers)
                    response = http.getresponse()
                    http.close()

                    if response.status != 200:
                        log.error('Couldn\'t upload to Xively -> ' +
                                  str(response.status) + ': ' + response.reason)

                except Exception as e:
                    log.error('HTTP error: ' + str(e))
